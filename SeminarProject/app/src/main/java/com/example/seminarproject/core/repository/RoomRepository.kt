package com.example.seminarproject.core.repository

import android.app.Application
import com.example.seminarproject.core.data.db.RoomHelper
import com.example.seminarproject.core.data.db.RoomMemo
import com.example.seminarproject.core.data.db.RoomMemoDao
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class RoomRepository @Inject constructor(application: Application) {
    private val roomMemoDao : RoomMemoDao by lazy {
        val db = RoomHelper.getDatabase(application)
        db.roomMemoDao()
    }

    fun getAll() : List<RoomMemo>{
        return roomMemoDao.getAll()
    }

    fun insert(memo: RoomMemo) : Observable<Unit>{
        return Observable.fromCallable { roomMemoDao.insert(memo) }
            //.roomMemoDao.insert(memo)
    }

    fun delete(memo: RoomMemo) : Observable<Unit>{
        return Observable.fromCallable { roomMemoDao.delete(memo) }
        //roomMemoDao.delete(memo)
    }

}