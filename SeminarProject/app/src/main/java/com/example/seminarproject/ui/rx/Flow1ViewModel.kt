package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class Flow1ViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"Flo1ViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        var data = ""
        val disposable : Disposable = Flowable.range(1, 100)
            .map { item ->
                item.also {
                    println("아이템 발행: $item")
                    data += "아이템 발행 : $item\n"
                }
            }
            .observeOn(Schedulers.io())
            .subscribe { item ->
                println("아이템 소비 : $item")
                data += "아이템 소비 : $item\n"
            }
        disposable.dispose()

        _currentValue.value = data
    }
}