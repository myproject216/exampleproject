package com.example.seminarproject.ui.rx

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FlowableViewModel @Inject constructor() : ViewModel(){
}