package com.example.seminarproject.ui.rx

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CallbackViewModel @Inject constructor() : ViewModel(){

    val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(input : String){
        _currentValue.value = input
    }
}