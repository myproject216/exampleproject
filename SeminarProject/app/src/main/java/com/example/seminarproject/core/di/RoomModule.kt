package com.example.seminarproject.core.di

import com.example.seminarproject.core.data.db.RoomHelper
import com.example.seminarproject.core.data.db.RoomMemoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideRoomMemoDao(db : RoomHelper) : RoomMemoDao{
        return db.roomMemoDao()
    }
}