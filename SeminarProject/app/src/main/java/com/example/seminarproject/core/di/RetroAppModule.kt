package com.example.seminarproject.core.di

import com.example.seminarproject.core.data.api.LibraryOpenApi
import com.example.seminarproject.core.data.api.LibraryOpenService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetroAppModule {

    @Singleton
    @Provides
    internal fun provideRetrofit(): LibraryOpenService {
        val retrofit = Retrofit.Builder()
            .baseUrl(LibraryOpenApi.DOMAIN)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(LibraryOpenService::class.java)
    }

}