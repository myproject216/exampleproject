package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class FlatMap1ViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"FlatMap1ViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue() {

        val balls = arrayOf("1", "2", "3", "4", "5")
        // flatMap은 flatMap 블럭의 Observable을 병합하여 한번에 방출하기 때문에 각각의 dealy에 따라 방출
        val disposable : Disposable = Observable.fromArray(*balls)
            .flatMap { ball -> Observable.just("$ball<>", "<>$ball") }
            .subscribe {
                _currentValue.value = _currentValue.value.toString() + "\n" + it
            }
            disposable.dispose()
    }
}