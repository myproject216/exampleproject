package com.example.seminarproject.ui.retrofit

import android.content.ContentProvider
import android.content.Context
import android.media.metrics.Event
import android.util.EventLog
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.seminarproject.core.data.api.LibraryOpenApi
import com.example.seminarproject.core.data.api.LibraryOpenService
import com.example.seminarproject.core.di.RetroAppModule
import com.example.seminarproject.core.model.Library
import com.example.seminarproject.core.repository.RetroRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.internal.operators.single.SingleDoOnEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RetrofitViewModel @Inject constructor() : ViewModel(){

    val libraryListLiveData = MutableLiveData<Library>()
    val libraryList : LiveData<Library>
        get() = libraryListLiveData

    @Inject
    lateinit var retro : RetroRepository

    // Retrofit 부분
    fun loadLibrary(){

        retro
            .getLibrary(LibraryOpenApi.API_KEY)
            .enqueue(object : Callback<Library> {
                // 서버 요청 실패 시 Toast 메세지
                override fun onFailure(call: Call<Library>, t: Throwable) {
                    //Toast.makeText(this,"서버에 데이터를 가져올 수 없습니다.", Toast.LENGTH_LONG).show()
                }
                // 서버 데이터를 정상적으로 받았을 때 지도에 마커를 표시하는 메서드 호출
                override fun onResponse(call: Call<Library>, response: Response<Library>) {
                    libraryListLiveData.value = response.body() as Library
                }
            })
    }


}