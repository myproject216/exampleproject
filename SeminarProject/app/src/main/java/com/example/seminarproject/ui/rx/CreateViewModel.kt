package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject


@HiltViewModel
class CreateViewModel @Inject constructor() : ViewModel(){

    companion object{
        const val TAG : String = "로그"
    }

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(TAG,"CreateViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        val disposable : Disposable = Observable.create<String> { emitter ->
            emitter.onNext("Hello")
            emitter.onNext("RxJava")
            emitter.onComplete()
        }
            .subscribe { _currentValue.value = _currentValue.value.toString() + "\n" + it.toString() }
            disposable.dispose()
    }
}