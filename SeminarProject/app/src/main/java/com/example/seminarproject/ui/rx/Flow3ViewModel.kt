package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.BackpressureOverflowStrategy
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.operators.flowable.FlowableBlockingSubscribe.subscribe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class Flow3ViewModel @Inject constructor() : ViewModel() {

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"Flo1ViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue() {

        val disposable: Disposable = Flowable.create<Int>({ emitter ->
            (0..10000).forEach {
                //다운스트림 취소 및 폐기 시 true
                if (emitter.isCancelled) {
                    return@create
                }
                emitter.onNext(it)
            }
            emitter.onComplete()
        }, BackpressureStrategy.DROP)
            .subscribeOn(Schedulers.computation())
            .observeOn(Schedulers.io())
            .subscribe(::println) { throwable ->
                throwable.printStackTrace()
            }
    }

/*
        // Flowable 의 다른 생성 연산자에서는 전략을 주지 않아도 됨
        val disposable1 : Disposable = Flowable.just(1,2,3,4)
             .subscribe(
                { _currentValue.value = _currentValue.value.toString() + "onNext: " + it }, // onNext: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonError: " }, // onError: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonComplete: " }, // onComplete: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonSubscribe: " } // onSubscribe: Consumer
            )
 */


/*
        // onBackPressureBuffer 사용
        val disposable: Disposable = Flowable.range(1,1000)
            .onBackpressureBuffer(128,{
                // 오버플로우가 발생했을 때의 동작 정의
                println("Error")
            }, BackpressureOverflowStrategy.DROP_LATEST)
            .observeOn(Schedulers.io())
            .subscribe({
                Thread.sleep(100)
                println("Emit : $it")
            }, { it : Throwable -> it.printStackTrace()})
        Thread.sleep((100*10).toLong())
 */

/*
       // onBackPressureDrop 사용
        val disposable : Disposable = Flowable.range(1,1000000)
            .onBackpressureDrop()
            .observeOn(Schedulers.io())
            .subscribe({
                Thread.sleep(100)
                println("Emit : $it")
            }, {it: Throwable -> it.printStackTrace()})
        Thread.sleep(20_000)

*/

/*
        // onBackPressureLatest 사용
        val disposable : Disposable = Flowable.range(1,1000000)
            .onBackpressureLatest()
            .doOnNext{ println("Emit: $it")}
            .observeOn(Schedulers.io())
            .subscribe{
                Thread.sleep(100)
                println("Consumer : $it")
            }
        Thread.sleep((100*1000).toLong())
 */

}
