package com.example.seminarproject.ui.room

import android.app.Application
import androidx.lifecycle.ViewModel
import com.example.seminarproject.core.data.db.RoomMemo
import com.example.seminarproject.core.repository.RoomRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RoomViewModel @Inject constructor(application: Application) : ViewModel(){

    private val disposable : CompositeDisposable = CompositeDisposable()

    private val repository : RoomRepository by lazy {
        RoomRepository(application)
    }

    fun getAll() = library

    private val library : List<RoomMemo> by lazy {
        repository.getAll()
    }

    fun insert(memo: RoomMemo, next: () -> Unit){
        disposable.add( repository.insert(memo).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { next() }
        )
    }

    fun delete(memo: RoomMemo, next: () -> Unit) {
        disposable.add(repository.delete(memo).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ next() }
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}


