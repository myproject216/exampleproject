package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class Flow2ViewModel @Inject constructor() : ViewModel() {

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"Flo2ViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        val disposable : Disposable = Flowable.interval(10, TimeUnit.MILLISECONDS)
            //.onBackpressureDrop()
            .observeOn(Schedulers.io())
            .map { item ->
                Thread.sleep(2000)
                item.also {
                    println("아이템 발행: $item")
                }
            }
            .subscribe({ item ->
                Thread.sleep(2000)
                println("아이템 소비 : $item")
            }) { throwable ->
                throwable.printStackTrace()
            }
    }
}