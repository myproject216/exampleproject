package com.example.seminarproject.core.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// 해당 테이블에 객체를 맵핑하기 위해서 Entity 를 선언
@Entity(tableName = "room_memo")
class RoomMemo {
    // ColumnInfo 선언으로 해당 프로퍼티를 테이블의 컬럼으로 사용된다는 것을 명시
    // PrimaryKey 를 지정해주고 옵션으로 autoGenerate 를 true 로 지정하면  Key 가 자동증가 하도록 해줌
    @PrimaryKey(autoGenerate = true)    // 각 레코드를 구분하기 위해서는 기본키를 지정해줘야 함
    @ColumnInfo
    var no: Long? = null

    @ColumnInfo
    var content: String = ""    // 내용

    @ColumnInfo(name = "date")  // 이름을 지정하면 해당이름으로 칼럼 생성
    var datetime: Long = 0  // 날짜

    constructor(content: String, datetime: Long) {
        this.content = content
        this.datetime = datetime
    }
}