package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentEmptyFlatMapBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmptyFlatMapFragment : Fragment() {

    private lateinit var binding: FragmentEmptyFlatMapBinding
    lateinit var emptyFlatMapViewModel: EmptyFlatMapViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        emptyFlatMapViewModel = ViewModelProvider(this).get(EmptyFlatMapViewModel::class.java)
        binding = FragmentEmptyFlatMapBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        emptyFlatMapViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.emflatText.text = it.toString()
        })

        binding.emflatBtn.setOnClickListener {
            emptyFlatMapViewModel.updateValue()
        }
    }
}