package com.example.seminarproject.ui.hilt

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentHiltBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HiltFragment : Fragment() {

    private lateinit var binding : FragmentHiltBinding

    lateinit var hiltViewModel: HiltViewModel

    @Inject
    lateinit var test : String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        hiltViewModel = ViewModelProvider(this).get(HiltViewModel::class.java)
        binding = FragmentHiltBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        hiltViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.textView.text = it

        })

        binding.HiltBtn.setOnClickListener {

            hiltViewModel.updateValue(test.toString())
            Log.d(TAG, "출력 : $test")

        }
    }


}