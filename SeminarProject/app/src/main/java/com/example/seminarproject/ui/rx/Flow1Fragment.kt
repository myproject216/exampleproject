package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentFlow1Binding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Flow1Fragment : Fragment() {

    private lateinit var binding: FragmentFlow1Binding
    lateinit var flow1ViewModel: Flow1ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flow1ViewModel = ViewModelProvider(this).get(Flow1ViewModel::class.java)
        binding = FragmentFlow1Binding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        flow1ViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.flo1Text.text = it.toString()
        })

        binding.flo1Btn.setOnClickListener {
            flow1ViewModel.updateValue()
        }
    }
}