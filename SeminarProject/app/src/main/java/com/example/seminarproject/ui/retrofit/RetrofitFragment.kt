package com.example.seminarproject.ui.retrofit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.R
import com.example.seminarproject.core.data.api.LibraryOpenApi
import com.example.seminarproject.core.data.api.LibraryOpenService
import com.example.seminarproject.databinding.FragmentRetrofitBinding
import com.example.seminarproject.core.model.Library
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.System.loadLibrary
import javax.inject.Inject

@AndroidEntryPoint
class RetrofitFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mView: MapView
    private lateinit var mMap : GoogleMap
    private lateinit var binding: FragmentRetrofitBinding
    lateinit var retrofitViewModel: RetrofitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        retrofitViewModel = ViewModelProvider(this).get(RetrofitViewModel::class.java)
        binding = FragmentRetrofitBinding.inflate(inflater,container,false)

        val rootView = binding.root
        mView = rootView.findViewById(R.id.mapView)
        mView.onCreate(savedInstanceState)
        mView.getMapAsync(this)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        retrofitViewModel.libraryListLiveData.observe(viewLifecycleOwner, ::showLibraries)
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap
        retrofitViewModel.loadLibrary()
    }

    // RxJava 사용하여 map 위에 marker 찍는 부분
    fun showLibraries(libraries: Library) {

        val latLngBounds = LatLngBounds.Builder()
        // val list = libraries.SeoulPublicLibraryInfo.row

        for (lib in libraries.SeoulPublicLibraryInfo.row!!) {
            if (lib.XCNTS != null && lib.YDNTS != null) {
                // 마커의 좌표를 생성
                val position = LatLng(lib.XCNTS.toDouble(), lib.YDNTS.toDouble())
                // 좌표와 도서관 이름으로 마커 생성
                val marker = MarkerOptions().position(position).title(lib.LBRRY_NAME)
                // 마커를 지도에 추가
                mMap.addMarker(marker)

                latLngBounds.include(marker.position)
            }
        }

        try {
            val bounds = latLngBounds.build()
            val padding = 0
            val update = CameraUpdateFactory.newLatLngBounds(bounds, padding)
            mMap.moveCamera(update)
        }catch (e:Exception){
            Log.d("Tag",e.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        mView.onStart()
    }
    override fun onStop() {
        super.onStop()
        mView.onStop()
    }
    override fun onResume() {
        super.onResume()
        mView.onResume()
    }
    override fun onPause() {
        super.onPause()
        mView.onPause()
    }
    override fun onLowMemory() {
        super.onLowMemory()
        mView.onLowMemory()
    }
    override fun onDestroy() {
        mView.onDestroy()
        super.onDestroy()
    }

}