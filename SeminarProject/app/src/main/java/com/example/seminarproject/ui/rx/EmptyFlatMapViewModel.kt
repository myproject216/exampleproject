package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject


@HiltViewModel
class EmptyFlatMapViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"EmptyFlatMapViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        val disposable : Disposable = Observable.just("A", "", "B", "", "C", "")
            .flatMap { data ->
                if ("" == data) {
                    Observable.empty()
                } else {
                    Observable.just(data.lowercase())
                }
            }.subscribe{_currentValue.value = _currentValue.value.toString() + "\n" + it}
            disposable.dispose()

    }

}