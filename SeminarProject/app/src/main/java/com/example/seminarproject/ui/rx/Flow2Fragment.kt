package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentFlow2Binding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Flow2Fragment : Fragment() {

    private lateinit var binding: FragmentFlow2Binding
    lateinit var flow2ViewModel: Flow2ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flow2ViewModel = ViewModelProvider(this).get(Flow2ViewModel::class.java)
        binding = FragmentFlow2Binding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        flow2ViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.flo2Text.text = it.toString()
        })

        binding.flo2Btn.setOnClickListener {
            flow2ViewModel.updateValue()
        }
    }
}