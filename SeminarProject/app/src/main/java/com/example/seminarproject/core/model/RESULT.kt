package com.example.seminarproject.core.model

data class RESULT(

    val CODE: String?,
    val MESSAGE: String?

)