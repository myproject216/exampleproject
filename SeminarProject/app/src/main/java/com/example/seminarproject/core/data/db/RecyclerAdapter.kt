package com.example.seminarproject.core.data.db

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.seminarproject.databinding.ItemRecyclerBinding
import com.example.seminarproject.ui.room.RoomViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat

// 리사이클러뷰는 리사이클러뷰어댑터라는 메서드 어댑터를 사용해서 데이터를 연결함.
class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.Holder>() {

    var helper: RoomHelper? = null
    // 어댑터에서 사용할 데이터 목록 변수 선언
    var listData = mutableListOf<RoomMemo>()

    // 아이템 레이아웃 생성하는 메서드로 한 화면에 보이는 개수만큼 안드로이드가 이 메서드를 호출함.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = ItemRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)
    }

    // 리사이클러뷰에서 사용할 데이터의 총 개수를 리턴
    override fun getItemCount(): Int {
        return listData.size
    }

    // 생성된 뷰홀더를 화면에 보여주는 메서드
    override fun onBindViewHolder(holder: Holder, position: Int) {
        val memo = listData.get(position)
        holder.setRoomMemo(memo)
    }

    inner class Holder(val binding: ItemRecyclerBinding) : RecyclerView.ViewHolder(binding.root) {
        var mRoomMemo: RoomMemo? = null
        init {
            // 삭제 버튼 클릭 시 이벤트
            binding.buttonDelete.setOnClickListener {
                helper?.roomMemoDao()?.delete(mRoomMemo!!)
                listData.remove(mRoomMemo)
                notifyDataSetChanged()
            }
        }
        // 홀더 클래스에서 화면에 데이터를 세팅하는 메서드
        fun setRoomMemo(memo: RoomMemo) {
            binding.textNo.text = "${memo.no}"
            binding.textContent.text = memo.content
            val sdf = SimpleDateFormat("yyyy/MM/dd hh:mm")
            // 날짜 포맷은 SimpleDateFormat 으로 설정
            binding.textDatetime.text = "${sdf.format(memo.datetime)}"

            this.mRoomMemo = memo
        }
    }
}