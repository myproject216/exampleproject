package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentErrorFlatMapBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ErrorFlatMapFragment : Fragment() {

    private lateinit var binding: FragmentErrorFlatMapBinding
    lateinit var errorFlatMapViewModel: ErrorFlatMapViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        errorFlatMapViewModel = ViewModelProvider(this).get(ErrorFlatMapViewModel::class.java)
        binding = FragmentErrorFlatMapBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        errorFlatMapViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.errFlatText.text = it.toString()
        })

        binding.errFlatBtn.setOnClickListener {
            errorFlatMapViewModel.updateValue()
        }
    }
}