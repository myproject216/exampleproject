package com.example.seminarproject.core.repository

import android.app.Application
import com.example.seminarproject.core.data.api.LibraryOpenService
import com.example.seminarproject.core.di.RetroAppModule
import com.example.seminarproject.core.model.Library
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject

class RetroRepository @Inject constructor(
    private val libraryOpenService: LibraryOpenService
){
    fun getLibrary(key : String) : Call<Library> {
        return  libraryOpenService.getLibrary(key)
    }

}