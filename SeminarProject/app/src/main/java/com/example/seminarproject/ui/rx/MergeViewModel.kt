package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class MergeViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"MergeViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        val ob1 = io.reactivex.Observable.just(1,2)
        val ob2 = io.reactivex.Observable.just(3,4)

        // 각각 Observable 을 단순히 합치는 연산자
        val disposable : Disposable = Observable.merge(ob1, ob2)
            .subscribe{ _currentValue.value = _currentValue.value.toString() + it }
            disposable.dispose()
    }
}